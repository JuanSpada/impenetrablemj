<?php require_once('partials/header.php'); ?>

<div class="row-fluid page_title">

    <div class="container">
        <div class="span12">
            <h2 class="title_size">
                <!-- Contact -->
                <span class="title_labeled">Contacto</span>
            </h2>
            <h2 class="title_desc">
                Deje su consulta y dentro de poco sera contactado por uno de nuestros representantes.
            </h2>
        </div>
        <!-- <div class="span4">
            <div class="pull-right">

                <div id="search-3" class="widget title_widget widget_search">
                    <form class="form-inline" action="http://pixel.themeple.co/" id="searchform" method="get">

                        <input placeholder="Search type here.." class="span11 search-query" name="s" id="s" size="25"  type="text" />
                        <button type="submit" class="search_icon"></button>
                    </form>
                    <span class="seperator extralight-border"></span>
                </div>
            </div>
        </div> -->

    </div>

    <div class="row-fluid divider base_color_background">
        <div class="container">
            <span class="bottom_arrow"></span>
        </div>
    </div>

</div>
<div class="container shadow">
    <span class="bottom_shadow_full"></span>
</div>
<div class="container">
    <div class="row-fluid distance_1">

        <div class="span12">
            
            <div class="row-fluid">
                <div class="span8 sc-col">
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="recent_title">
                                <h2>Dejenos su consulta</h2>
                            </div>
                            <span class="row-fluid separator_border"></span>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <form name="contact-form" id="contact-form" class="standard-form" action="contact.php" method="POST">
                            <input class="span6" placeholder="Nombre" name="name" type="text" id="name" value="" required />
                            <input class="span6" placeholder="E-Mail" name="email" type="text" id="email" value="" required/>
                            <input class="span12" placeholder="Teléfono" name="phone" type="text" id="phone" value=""/>
                            <textarea class="span12" placeholder="Mensaje" name="message" cols="40" rows="7" id="message" required></textarea>
                            <p>
                                <input type="submit"  value="Enviar" class="button_bar" />
                            </p>
                        </form>
                        <div id="ajaxresponse"></div>
                    </div>
                </div>
                <div class="span4 sc-col">
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="recent_title">
                                <h2>Datos de Contacto</h2>
                            </div>
                            <span class="row-fluid separator_border"></span>
                        </div>
                    </div>
                    <div class="row-fluid">
                        <div class="span12 sc-col">
                            <!-- <h1>ImpenetrableMJ</h1> -->
                            <p>Buenos Aires, Argentina</p>
                        
                        <p>Teléfonos: 011 3162 2890 | 011 6198 0659</p>
                        <p>
                            Email:
                            <a style="color: #666666;" href="mailto:contacto@impenetrablemj.com.ar">contacto@impenetrablemj.com.ar</a>
                            <br>    
                            Web:
                            <a style="color: #666666;" href="https://www.impenetrablemj.com.ar">www.impenetrablemj.com.ar</a></p>
                        </div>
                    </div>
                
                
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="recent_title">
                                <h2>Más Información</h2>
                            </div>
                            <span class="row-fluid separator_border"></span>
                        </div>
                    </div>
                    <strong style="color: #666666;">Horarios de atención:</strong>
                    <br>
                    <p style="color: #666666;">Lunes a Viernes de 9 a 18.</p>
                </div>
            </div>
            <div class="row-fluid">
                <iframe class="googlemap" frameborder="0" scrolling="no" marginheight="0" marginwidth="0" src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d81831.3940625949!2d-61.02136242232752!3d-24.984111711842136!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x946add0341e09b9b%3A0x13e22107147a1b53!2sParque+Nacional+El+Impenetrable!5e0!3m2!1ses!2sar!4v1566334019268!5m2!1ses!2sar"></iframe>
                <div class="row-fluid">
                    <div class="bottom_shadow_full googlemap_shadow" style="background-color: #dcdcdc"></div>
                </div>
            </div>
            </p>

        </div>

    </div>
</div>
<?php
require_once('partials/footer.php');
?>
<script>
    document.getElementById("nav4").classList.add('current-menu-item', 'current_page_item');
</script>