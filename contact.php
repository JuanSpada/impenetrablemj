<?php

$name = isset($_POST['name']) ? $_POST['name'] : '';
$email = isset($_POST['email']) ? $_POST['email'] : '';
$phone = isset($_POST['phone']) ? $_POST['phone'] : '';
$message = isset($_POST['message']) ? $_POST['message'] : '';
$utm_source = isset($_POST['utm_source']) ? $_POST['utm_source'] : 'none';
$utm_term = isset($_POST['utm_term']) ? $_POST['utm_term'] : 'none';
$utm_campaign = isset($_POST['utm_campaign']) ? $_POST['utm_campaign'] : 'none';

$url = "https://hooks.zapier.com/hooks/catch/803715/o8r5o04/";

$data = array(
    'nombre' => $name,
    'email' => $email,
    'phone' => $phone,
    'message' => $message,
    'utm_source' => $utm_source,
    'utm_medium' => $utm_medium,
    'utm_campaign' => $utm_campaign,
    'utm_term' => $utm_term,
    'utm_content' => $utm_content,
);

$options = array(
    'http' => array(
        'header'  => "Content-type: application/x-www-form-urlencoded\r\n",
        'method'  => 'POST',
        'content' => http_build_query($data)
    )
);
$context  = stream_context_create($options);
$result = file_get_contents($url, false, $context);
if ($result === FALSE) { }