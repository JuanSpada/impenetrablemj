<?php
require_once('partials/header.php');
?>

    <div class="row-fluid page_title">

        <div class="container">
            <div class="span8">
                <h2 class="title_size">
                    <span class="title_labeled">Productos</span>
                </h2>
                <!-- <h2 class="title_desc">
                    Leña de Quebracho Colorado.
                </h2> -->
            </div>
            <div class="span4">
                <div class="pull-right">

                    <div id="search-3" class="widget title_widget widget_search">
                        <!-- <form class="form-inline" action="http://pixel.themeple.co/" id="searchform" method="get">

                            <input placeholder="Search type here.." class="span11 search-query" name="s" id="s" size="25"  type="text" />
                            <button type="submit" class="search_icon"></button>
                        </form> -->
                        <span class="seperator extralight-border"></span>
                    </div>
                </div>
            </div>

        </div>
        <div class="row-fluid divider base_color_background">
            <div class="container">
                <span class="bottom_arrow"></span>
            </div>
        </div>

    </div>
    <div class="row-fluid">
        <div class="container">

            <div class="row-fluid distance_1">

                <div class="span12">
                    <div id="holder" class="portfolio-grid columns2 distance_1 ">
                        <div class=" distance_2">
                            <div class="filterable">
                                <div class="html  box_port">
                                    <div class="slideshow_container flexslider slide_layout_" >
                                        <ul class="slides slide_flexslider">
                                            <li class=' slide_element slide2 frame2'>
                                                <img style="height: 265px;" src='images/productos/hogar/puertas.jpg'/>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="row-fluid divider base_color_background">
                                        <span class="top_arrow"></span>
                                        <h4 class="portfolio_title">
                                            <a href="products.php?c=11">Puertas</a>
                                        </h4>
                                    </div>
                                </div>
                                
                                <div class="html  box_port">
                                    <div class="slideshow_container flexslider slide_layout_" >
                                        <ul class="slides slide_flexslider">
                                            <li class=' slide_element slide1 frame1'>
                                                <img style="height: 265px;" src='images/productos/hogar/ventanas.jpg'/>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="row-fluid divider base_color_background">
                                        <span class="top_arrow"></span>
                                        <h4 class="portfolio_title">
                                            <a href="products.php?c=12">Ventanas</a>
                                        </h4>
                                    </div>
                                </div>
                                <div class="html  box_port">
                                    <div class="slideshow_container flexslider slide_layout_" >
                                        <ul class="slides slide_flexslider">
                                            <li class=' slide_element slide1 frame1'>
                                                <img style="height: 265px;" src='images/productos/hogar/pergolas1.jpg' title='' alt='' />
                                            </li>
                                            <li class=' slide_element slide1 frame1'>
                                                <img style="height: 265px;" src='images/productos/hogar/pergolas2.jpg' title='' alt='' />
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="row-fluid divider base_color_background">
                                        <span class="top_arrow"></span>
                                        <h4 class="portfolio_title">
                                            <a href="products.php?c=13">Pérgolas</a>
                                        </h4>
                                    </div>
                                </div>
                            
                            <div class="html  box_port">
                                    <div class="slideshow_container flexslider slide_layout_" >
                                        <ul class="slides slide_flexslider">
                                            <li class=' slide_element slide1 frame1'>
                                                <img style="height: 265px;" src='images/productos/hogar/deck.jpg' title=''/>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="row-fluid divider base_color_background">
                                        <span class="top_arrow"></span>
                                        <h4 class="portfolio_title">
                                            <a href="products.php?c=15">Deck de Madera</a>
                                        </h4>
                                    </div>
                                </div> 
                                <div class="html  box_port">
                                    <div class="slideshow_container flexslider slide_layout_" >
                                        <ul class="slides slide_flexslider">
                                            <li class=' slide_element slide1 frame1'>
                                                <img style="height: 265px;" src='images/productos/hogar/postes.jpg' title=''/>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="row-fluid divider base_color_background">
                                        <span class="top_arrow"></span>
                                        <h4 class="portfolio_title">
                                            <a href="products.php?c=16">Postes para Alambrar</a>
                                        </h4>
                                    </div>
                                </div> 
                                <div class="html  box_port">
                                    <div class="slideshow_container flexslider slide_layout_" >
                                        <ul class="slides slide_flexslider">
                                            <li class=' slide_element slide1 frame1'>
                                                <img style="height: 265px;" src='images/productos/hogar/mediodurmiente.jpg' title=''/>
                                            </li>
                                            <li class=' slide_element slide1 frame1'>
                                                <img style="height: 265px;" src='images/productos/hogar/durmiente.jpg' title=''/>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="row-fluid divider base_color_background">
                                        <span class="top_arrow"></span>
                                        <h4 class="portfolio_title">
                                            <a href="products.php?c=17">Durmientes y Medio Durmientes</a>
                                        </h4>
                                    </div>
                                </div> 
                                <div class="html  box_port">
                                    <div class="slideshow_container flexslider slide_layout_" >
                                        <ul class="slides slide_flexslider">
                                            <li class=' slide_element slide1 frame1'>
                                                <img style="height: 265px;" src='images/productos/hogar/tranquera.jpg' title=''/>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="row-fluid divider base_color_background">
                                        <span class="top_arrow"></span>
                                        <h4 class="portfolio_title">
                                            <a href="products.php?c=18">Tranqueras</a>
                                        </h4>
                                    </div>
                                </div>
                                <div class="html  box_port">
                                    <div class="slideshow_container flexslider slide_layout_" >
                                        <ul class="slides slide_flexslider">
                                            <li class=' slide_element slide1 frame1'>
                                                <img style="height: 265px;" src='images/productos/hogar/tabla.jpg' title=''/>
                                            </li>
                                        </ul>
                                    </div>
                                    <div class="row-fluid divider base_color_background">
                                        <span class="top_arrow"></span>
                                        <h4 class="portfolio_title">
                                            <a href="products.php?c=14">Tablas</a>
                                        </h4>
                                    </div>
                                </div>  
                                
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type='text/javascript'>
 /* <![CDATA[ */  
var portfolio_options = {
    nr_columns: '2', 
    }; 
 /* ]]>*/ </script>

<?php
require_once('partials/footer.php');
?>