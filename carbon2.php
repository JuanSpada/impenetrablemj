<?php
require_once('partials/header.php');
?>

    <div class="row-fluid page_title">

        <div class="container">
            <div class="span8">
                <h2 class="title_size">
                    <span class="title_labeled">Productos</span>
                </h2>
                <!-- <h2 class="title_desc">
                    Leña de Quebracho Colorado.
                </h2> -->
            </div>
            <div class="span4">
                <div class="pull-right">

                    <div id="search-3" class="widget title_widget widget_search">
                        <!-- <form class="form-inline" action="http://pixel.themeple.co/" id="searchform" method="get">

                            <input placeholder="Search type here.." class="span11 search-query" name="s" id="s" size="25"  type="text" />
                            <button type="submit" class="search_icon"></button>
                        </form> -->
                        <span class="seperator extralight-border"></span>
                    </div>
                </div>
            </div>

        </div>
        <div class="row-fluid divider base_color_background">
            <div class="container">
                <span class="bottom_arrow"></span>
            </div>
        </div>

    </div>
    <div class="row-fluid">
        <div class="container">


                    <div id="holder" class="portfolio-grid columns2 distance_1 d-flex">

                        <div class="row">

                            <div class="html  box_port col-6">
                                <div class="slideshow_container flexslider slide_layout_" >
                                    <ul class="slides slide_flexslider">
                                        <li class=' slide_element slide2 frame2'>
                                            <img src='images/productos/carbon/1.jpg' title='photodune-220303-panoramic-view-of-beautiful-skyscrapers-and-river-s' alt='Panoramic view of beautiful architectural skyscrapers and river' />
                                        </li>
                                    </ul>
                                </div>
                                <div class="row-fluid divider base_color_background">
                                    <span class="top_arrow"></span>
                                    <h4 class="portfolio_title">
                                        <a href="products.php?c=1">Bolsas de 3, 4, 5, 8 y 10 kg</a>
                                    </h4>
                                </div>
                            </div>
    
                            <div class="html  box_port col-6">
                                <div class="slideshow_container flexslider slide_layout_" >
                                    <ul class="slides slide_flexslider">
                                        <li class=' slide_element slide1 frame1'>
                                            <img src='images/productos/carbon/3.jpg' title='photodune-220303-panoramic-view-of-beautiful-skyscrapers-and-river-s' alt='Panoramic view of beautiful architectural skyscrapers and river' />
                                        </li>
                                    </ul>
                                </div>
                                <div class="row-fluid divider base_color_background">
                                    <span class="top_arrow"></span>
                                    <h4 class="portfolio_title">
                                        <a href="products.php?c=2">Bolsas de 35 kg</a>
                                    </h4>
                                </div>
                            </div>
    
                            <div class="html  box_port col-6">
                                <div class="slideshow_container flexslider slide_layout_" >
                                    <ul class="slides slide_flexslider">
                                        <li class=' slide_element slide1 frame1'>
                                            <img src='images/productos/carbon/4.jpg' title='City Skyline' alt='Modern city downtown skyline view' />
                                        </li>
                                    </ul>
                                </div>
                                <div class="row-fluid divider base_color_background">
                                    <span class="top_arrow"></span>
                                    <h4 class="portfolio_title">
                                        <a href="products.php?c=3">1000 kg</a>
                                    </h4>
                                </div>
                            </div>
    
                            <div class="html  box_port col-6">
                                <div class="slideshow_container flexslider slide_layout_" >
                                    <ul class="slides slide_flexslider">
                                        <li class=' slide_element slide1 frame1'>
                                            <img src='images/productos/carbon/2.jpg' title='City Skyline' alt='Modern city downtown skyline view' />
                                        </li>
                                    </ul>
                                </div>
                                <div class="row-fluid divider base_color_background">
                                    <span class="top_arrow"></span>
                                    <h4 class="portfolio_title">
                                        <a href="products.php?c=4">Equipo Completo (28 Toneladas)</a>
                                    </h4>
                                </div>
                            </div>  
                        </div>


                    </div>
                </div>
            </div>
        </div>
    </div>

    <script type='text/javascript'>
 /* <![CDATA[ */  
var portfolio_options = {
    nr_columns: '2', 
    }; 
 /* ]]>*/ </script>

<?php
require_once('partials/footer.php');
?>