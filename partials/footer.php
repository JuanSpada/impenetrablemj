<div class="footers row-fluid pull-left distance_1">
            <div class="row-fluid ">
                <div class="span12 page_top_header line-divider"></div>
            </div>
            <footer class="row-fluid ">

            <div class="row-fluid">
                <div class="span12">
                    <div class="container">
                        <div class="row-fluid">
                            <div class="span6" style="display: flex; justify-content: center;">

                                <div  class="widget widget_about_social">
                                    <div class="footer_title">
                                        <h2 class="widget-title">Sobre Impenetrable MJ</h2>
                                    </div>
                                    <div>
                                        <p>
                                        Somos una empresa constituida a principios del 2019 con esfuerzo y dedicación. Nos dedicamos a la venta y proveeduría de leña y carbón como también a diversos productos derivados de distintos tipos de maderas, especialmente de Quebracho Colorado, estamos abocados en ofrecer productos de calidad y el mejor servicio del mercado.
                                        </p>
                                        <div class="row-fluid">
                                            <span class="social_icon">
                                                <a href="https://www.facebook.com/impenetrable.mj.9">
                                                    <span class="facebook"></span>
                                                </a>
                                            </span>
                                            <span class="social_icon">
                                                <a href="https://www.instagram.com/impenetrablemjok">
                                                    <span class="instagram"></span>
                                                </a>
                                            </span>
                                                <!-- <span class="social_icon">
                                                    <a href="#">
                                                        <span class="twitter"></span>
                                                    </a>
                                                </span>
                                                <span class="social_icon">
                                                    <a href="#">
                                                        <span class="vimeo"></span>
                                                    </a>
                                                </span>
                                                <span class="social_icon">
                                                    <a href="#">
                                                        <span class="dribbble"></span>
                                                    </a>
                                                </span> -->
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="span6" style="display: flex; justify-content: center;">

                                <div  class="widget widget_flickr d-flex justify-center">
                                    <div class="footer_title">
                                        <h2 class="widget-title">Productos destacados</h2>
                                    </div>
                                    <div class="row" style="display: flex; flex-direction: row;">
                                        <div class="">
                                            <a href="leña.php">
                                                <img src="images/slide2.jpg" alt="" style="margin-left: 20px;">
                                            </a>
                                        </div>
                                        <div class="">
                                            <a href="carbon.php">
                                                <img src="images/productos/carbon/3.jpg" alt="">
                                            </a>
                                        </div>
                                        <div class="">
                                            <a href="hogar.php">
                                                <img src="images/productos/hogar/tabla.jpg" alt="" style="transform: rotate(90deg)">
                                            </a>
                                        </div>
                                    </div>
                                        <!-- <script type="text/javascript" src="http://www.flickr.com/badge_code_v2.gne?count=6&amp;display=latest&amp;size=s&amp;layout=x&amp;source=user&amp;user=52617155@N08"></script> -->
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>

                </div>
            </div>
        </footer>

<div class="footers row-fluid pull-left" style="margin-top: 2px;">
    <div class="row-fluid base_color_background footer_copyright">
        <div class="span12">
            <div class="container">
                <span class="arrow row-fluid">
                    <span class="span12"></span>
                </span>
                <div class="row-fluid">

                    <div class="span6">
                        Copyright 2020 - Impenetrable MJ 
                    </div>
                    <div class="span6 pull-right">
                        <p class="pull-right">Powered By <a href="https://www.wemanagement.com.ar">We Management</a></p>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>

<script type='text/javascript' src="js/contact.js"></script>
<script type='text/javascript' src='LayerSlider/js/layerslider.kreaturamedia.jquery6e0e.js?ver=3.0.0'></script>
<script type='text/javascript' src='LayerSlider/js/jquery-easing-1.36f3e.js?ver=1.3.0'></script>
</body>

</html>