

    <!DOCTYPE html>    
    <html dir="ltr" lang="en-US">

    <head>
        <meta charset="UTF-8" />    

        <title>Impenetrable MJ</title>

        <!-- PRELOADER -->
        <!-- <script type='text/javascript' src='js/preloader.js'></script>
        <link rel="stylesheet" type="text/css" media="all" href="css/preloader.css" />   -->

        <link href="https://fonts.googleapis.com/css?family=Roboto+Condensed&display=swap" rel="stylesheet">
        <link rel="stylesheet" href="bootstrap/css/bootstrap.css" type="text/css" media="screen"/>    
        <link rel="stylesheet" href="css/flexslider.css" type="text/css" media="screen"/>    
        <link rel="stylesheet" href="css/mediaelementplayer.css" type="text/css" media="screen"/>    
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">    
        <meta name="viewport" content="width=device-width, user-scalable=no">
        
        <link rel="stylesheet" type="text/css" media="all" href="style.css" />  
        <link rel="stylesheet" href="bootstrap/css/bootstrap-responsive.css" type="text/css" media="screen"/>     
        <link rel='stylesheet' id='layerslider_css-css'  href='LayerSlider/css/layerslider6e0e.css?ver=3.0.0' type='text/css' media='all' />    
        <script type='text/javascript' src='js/jquery.js?ver=1.7.2'></script>
        <script type='text/javascript' src='bootstrap/js/bootstrap68b3.js?ver=1'></script>
        <script type='text/javascript' src='js/jquery.easing.1.168b3.js?ver=1'></script>
        <script type='text/javascript' src='js/jquery.easing.1.368b3.js?ver=1'></script>
        <script type='text/javascript' src='js/jquery.flexslider-min68b3.js?ver=1'></script>
        <script type='text/javascript' src='js/themeple68b3.js?ver=1'></script>
        <script type='text/javascript' src='js/jquery.pixel68b3.js?ver=1'></script>
        <script type='text/javascript' src='js/jquery.carouFredSel-6.0.6-packed68b3.js?ver=1'></script>
        <script type='text/javascript' src='js/jquery.mobilemenu68b3.js?ver=1'></script>
        <script type='text/javascript' src='js/isotope68b3.js?ver=1'></script>
        <script type='text/javascript' src='js/greyScale.min68b3.js?ver=1'></script>
        <script type='text/javascript' src='js/mediaelement-and-player.min68b3.js?ver=1'></script>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- SWEET ALERT 2 -->
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>

        <link rel="icon" type="image/png" href="images/favicon.png">

        <script type="text/javascript">

        // SLIDE 
        jQuery(document).ready(function(){
            jQuery("#layerslider_1").layerSlider({ 
                width               : '100%',
                height              : '400px',
                responsive          : false,
                autoStart           : true,
                pauseOnHover        : true,
                firstLayer          : 1,
                animateFirstLayer   : true,
                twoWaySlideshow     : true,
                keybNav             : true,
                touchNav            : true,
                imgPreload          : true,
                navPrevNext         : true,
                navStartStop        : false,
                navButtons          : false,
                skin                : 'defaultskin',
                skinsPath           : 'http://pixel.themeple.co/LayerSlider/skins/',
                globalBGColor       : 'transparent',
                yourLogo            : false,
                yourLogoStyle       : 'position: absolute; left: 10px; top: 10px; z-index: 99;',
                yourLogoLink        : false,
                yourLogoTarget      : '_self',

                loops               : 0,
                forceLoopNum        : true,
                autoPlayVideos      : true,


                autoPauseSlideshow  : 'auto',
                youtubePreview      : 'maxresdefault.jpg',

                cbInit              : function(element) { },
                cbStart             : function(data) { },
                cbStop              : function(data) { },
                cbPause             : function(data) { },
                cbAnimStart         : function(data) { },
                cbAnimStop          : function(data) { },
                cbPrev              : function(data) { },
                cbNext              : function(data) { }            });
    jQuery("#layerslider_2").layerSlider({ 
        width               : '100%',
        height              : '400px',
        responsive          : false,
        autoStart           : true,
        pauseOnHover        : true,
        firstLayer          : 1,
        animateFirstLayer   : true,
        twoWaySlideshow     : true,
        keybNav             : true,
        touchNav            : true,
        imgPreload          : true,
        navPrevNext         : true,
        navStartStop        : false,
        navButtons          : false,
        skin                : 'defaultskin',
        skinsPath           : 'http://pixel.themeple.co/LayerSlider/skins/',
        globalBGColor       : 'transparent',
        yourLogo            : false,
        yourLogoStyle       : 'position: absolute; left: 10px; top: 10px; z-index: 99;',
        yourLogoLink        : false,
        yourLogoTarget      : '_self',

        loops               : 0,
        forceLoopNum        : true,
        autoPlayVideos      : true,


        autoPauseSlideshow  : 'auto',
        youtubePreview      : 'maxresdefault.jpg',

        cbInit              : function(element) { },
        cbStart             : function(data) { },
        cbStop              : function(data) { },
        cbPause             : function(data) { },
        cbAnimStart         : function(data) { },
        cbAnimStop          : function(data) { },
        cbPrev              : function(data) { },
        cbNext              : function(data) { }            });
    jQuery("#layerslider_3").layerSlider({ 
        width               : '940px',
        height              : '400px',
        responsive          : false,
        autoStart           : true,
        pauseOnHover        : true,
        firstLayer          : 1,
        animateFirstLayer   : true,
        twoWaySlideshow     : true,
        keybNav             : true,
        touchNav            : true,
        imgPreload          : true,
        navPrevNext         : true,
        navStartStop        : false,
        navButtons          : false,
        skin                : 'defaultskin',
        skinsPath           : 'http://pixel.themeple.co/LayerSlider/skins/',
        globalBGColor       : 'transparent',
        yourLogo            : false,
        yourLogoStyle       : 'position: absolute; left: 10px; top: 10px; z-index: 99;',
        yourLogoLink        : false,
        yourLogoTarget      : '_self',

        loops               : 0,
        forceLoopNum        : true,
        autoPlayVideos      : true,


        autoPauseSlideshow  : 'auto',
        youtubePreview      : 'maxresdefault.jpg',

        cbInit              : function(element) { },
        cbStart             : function(data) { },
        cbStop              : function(data) { },
        cbPause             : function(data) { },
        cbAnimStart         : function(data) { },
        cbAnimStop          : function(data) { },
        cbPrev              : function(data) { },
        cbNext              : function(data) { }            });
    });
    </script>
    </head>
    <body class="home blog" >

    <!-- PRELOADER -->
    <!-- <div class="load">
        <hr/><hr/><hr/><hr/>
    </div> -->

    <a href="https://wa.me/5491161980659?text=Hola" class="whatsapp" target="_blank"> <i class="fa fa-whatsapp whatsapp-icon"></i></a>
        <div class="row-fluid ">
            <div class="span12 page_top_header base_color_background"></div>
        </div>
        <div class="container">

            <div class="row-fluid header_container">

                <div class="span3">
                    <a href='index.html'>
                        <img src=images/logo.png alt='' style="width: 300px"/>    
                    </a>
                </div>
                <div class="span9 menu">
                    <ul id="menu-pixel" class="menu">
                        <li id="nav1">
                            <a href="index.php">Inicio</a>
                        </li>
                        <li id="nav2">
                            <a href="aboutus.php">Quienes Somos?</a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="aboutus.php#bienvenidos">Nosotros</a>
                                </li>
                                <li>
                                    <a href="aboutus.php#servicios">Servicios</a>
                                </li>
                            </ul>
                        </li>
                        <li id="nav3">
                            <a href="#">Productos</a>
                            <ul class="sub-menu">
                                <li>
                                    <a href="carbon.php">Carbón</a>
                                </li>
                                <li>
                                    <a href="leña.php">Leña</a>
                                </li>
                                <li>
                                    <a href="hogar.php">Hogar</a>
                                </li>
                            </ul>
                        </li>
                        
                        <li id="nav4"> 
                            <a href="contactus.php">Contacto</a>
                        </li>
                    </ul>
                </div>

            </div>
        </div>
    <div class="row-fluid ">
        <div class="span12 page_top_header line-divider"></div>
    </div>