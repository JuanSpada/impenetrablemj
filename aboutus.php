<?php
require_once('partials/header.php');
?>

<div class="row-fluid page_title">

    <div class="container">
        <div class="span8">
            <h2 class="title_size">
                Quienes
                <span class="title_labeled">Somos</span>
            </h2>
            <!-- <h2 class="title_desc">ImpenetrableMJ</h2> -->
        </div>

    </div>
    <div class="row-fluid divider base_color_background">
        <div class="container">
            <span class="bottom_arrow"></span>
        </div>
    </div>

</div>

<div class="container shadow">
    <span class="bottom_shadow_full"></span>
</div>
<div class="row-fluid ">
    <div class="span12">
        <div class="container">
            <div class="row-fluid distance_1 page_layout">
                <div class="slideshow_container pixel_slider slide_layout_" >
                    <ul class="slides slide_pixel_slider">
                        <li class=' slide_element slide1 frame1'>
                            <div class="single_slide_info row">
                                <div class="span10 info_block">
                                    <h2>Impenetrable MJ</h2>
                                    <p>
                                        Excelencia y Calidad
                                    </p>
                                    <p>
                                        <a class="button_bar" href="#">Más Información</a>
                                    </p>
                                </div>
                                <div class="span2 arrows">
                                    <div class="arrow base_color_background">
                                        <span class="next"></span>
                                    </div>
                                    <div class="arrow base_color_background">
                                        <span class="previous"></span>
                                    </div>
                                </div>
                                <span class="bottom_shadow_small"></span>
                            </div>
                            <a href='#'>
                                <img src='images/Impenetrable 1.jpg' title='Belvedere of Tuscany' alt='Scenic view of typical Tuscany landscape' />
                            </a>
                        </li>
                        <li class=' slide_element slide2 frame2'>
                            <div class="single_slide_info row">
                                <div class="span10 info_block">
                                    <h2>Impenetrable MJ</h2>
                                    <p>
                                    Excelencia y Calidad
                                    </p>
                                    <p>
                                        <a class="button_bar" href="#">Más Información</a>
                                    </p>
                                </div>
                                <div class="span2 arrows">
                                    <div class="arrow base_color_background">
                                        <span class="next"></span>
                                    </div>
                                    <div class="arrow base_color_background">
                                        <span class="previous"></span>
                                    </div>
                                </div>
                                <span class="bottom_shadow_small"></span>
                            </div>
                            <a href='#'>
                                <img src='images/Impenetrable 2.jpg' title='photodune-202687-smiling-cute-business-woman-with-colleagues-at-the-back-m' alt='Beautiful young business executive with colleagues discussing in the background' />
                            </a>
                        </li>
                        <li class=' slide_element slide3 frame3'>
                            <div class="single_slide_info row">
                                <div class="span10 info_block">
                                    <h2>Impenetrable MJ</h2>
                                    <p>
                                    Excelencia y Calidad
                                    </p>
                                    <p>
                                        <a class="button_bar" href="#">Más Información</a>
                                    </p>
                                </div>
                                <div class="span2 arrows">
                                    <div class="arrow base_color_background">
                                        <span class="next"></span>
                                    </div>
                                    <div class="arrow base_color_background">
                                        <span class="previous"></span>
                                    </div>
                                </div>
                                <span class="bottom_shadow_small"></span>
                            </div>
                            <a href='#'>
                                <img src='images/Impenetrable 3.jpg' title='Road in desert' alt='Road in desert with motion blur' />
                            </a>
                        </li>
                        <li class=' slide_element slide4 frame4'>
                            <div class="single_slide_info row">
                                <div class="span10 info_block">
                                    <h2>Impenetrable MJ</h2>
                                    <p>
                                        Excelencia y Calidad
                                    </p>
                                    <p>
                                        <a class="button_bar" href="#">Más Información</a>
                                    </p>
                                </div>
                                <div class="span2 arrows">
                                    <div class="arrow base_color_background">
                                        <span class="next"></span>
                                    </div>
                                    <div class="arrow base_color_background">
                                        <span class="previous"></span>
                                    </div>
                                </div>
                                <span class="bottom_shadow_small"></span>
                            </div>
                            <a href='#'>
                                <img src='images/Impenetrable 4.jpg' title='photodune-2359457-extreme-ride-s' alt='' />
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="container slide_container_divider">
            <div class="row-fluid divider slide_divider base_color_background">
                <div class="container">
                    <span class="bottom_arrow"></span>
                </div>
            </div>
        </div>
        <!-- <div class="container ">
            <div class="row-fluid slide_content page_layout">
                <h3>Quienes Somos?</h3>
                
                    <div class="row-fluid">
                        <div class="span6 sc-col">
                            Somos una empresa joven dedicada principalmente a la venta y proveeduría de leña y
                            carbón, estamos abocados a ofrecer productos de calidad y el mejor servicio del
                            mercado.
                        </div>
                        <div class="span6 sc-col">
                            Nuestros principales objetivos son: Mejorar la atención al público del mercado, darte
                            siempre el mejor precio y por sobre todo superar nuestros límites constantemente
                            para brindarte el mejor servicio siempre.
                            Nuestros servicios se amoldan a la necesidad de cada uno de nuestros clientes para
                            poder brindarles la mejor experiencia.
                        </div>
                    </div>
                
            </div>
        </div> -->
        <div class="row-fluid">
            <div class="container">
                <div class="bottom_shadow_full"></div>
            </div>
        </div>

        <div class="container">
            <div class="row-fluid distance_1">
                <div class=" box_shadow box_layout">
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="recent_title">
                                <h2 id="bienvenidos">Bienvenidos</h2>
                            </div>
                            <span class="row-fluid separator_border"></span>
                        </div>
                        <div class="row-fluid">
                            <p>Somos una empresa constituida a principios del 2019 con esfuerzo y dedicación. Nos dedicamos a la venta y proveeduría de leña y carbón como también a diversos productos derivados de distintos tipos de maderas, especialmente de Quebracho Colorado, estamos abocados en ofrecer productos de calidad y el mejor servicio del mercado. </p>
                            <strong style="color: #666666;">Nuestros Objetivos:</strong>
                            <p>
                                <ul style="color: #666666;">
                                    <li>Brindar el mejor servicio en ventas y proveeduría de leña y carbón del mercado Por eso es importante para nosotros escuchar al cliente y aceptar desafíos.</li>
                                    <li>Optimizar los recursos para brindarte el mejor precio/calidad.</li>
                                    <li>Seleccionar con expertos en la materia que ejemplares debe ser preservados de la tala. Con el fin de contribuir al cuidado del medio ambiente.</li>
                                </ul>
                            </p>
                            <strong id="servicios" style="color: #666666;">Servicios:</strong>
                            <ul></ul>
                            <p>Nos dedicamos a la venta y distribución de leña y carbón, para cumplir con cada demanda contamos con un servicio especializado, ofrecemos productos fraccionados en bolsas (chicas, medianas y grandes)como también grandes cantidades. 
                            Las distancias no son importantes, ya que contamos con un servicio de envío a Capital y Gran Buenos aires.</p>
                            <p>Las distancias no son importantes, ya que contamos con un servicio de envío a Capital y Gran Buenos aires.</p>
                        </div>
                    </div>
                </div>
                <!-- <div class="span4 box_shadow box_layout">
                    <div class="row-fluid">
                        <div class="span12">
                            <div class="recent_title">
                                <h2>Our Skills</h2>
                            </div>
                            <span class="row-fluid separator_border"></span>
                        </div>
                        <div class="row-fluid">
                            <p>WordPress 90%</p>
                            <div class="progress    progress-success">
                                <div class="bar" style="width: 90%%;"></div>
                            </div>
                            <p>Web Design 70%</p>
                            <div class="progress    progress-success">
                                <div class="bar" style="width: 70%%;"></div>
                            </div>
                            <p>HTML/CSS 80%</p>
                            <div class="progress    progress-success">
                                <div class="bar" style="width: 80%%;"></div>
                            </div>
                            <p>Print 30%</p>
                            <div class="progress    progress-success">
                                <div class="bar" style="width: 30%%;"></div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
        </div>

        <!-- <div class="container">
            <div class="row-fluid distance_1">
                <div class="row-fluid">
                    <div class="span12">
                        <div class="recent_title">
                            <h2>Our Staff</h2>
                        </div>
                        <span class="row-fluid separator_border"></span>
                    </div>
                </div>
                <div class="row-fluid">
                    <div class="span4 box_shadow box_layout">
                        <div class="staff_column">
                            <div class="recent_box">
                                <img alt="" src="images/2012/10/photodune-202599-smart-business-man-with-a-laptop-and-colleagues-at-the-back-xs1.jpg" />
                                <div class="row-fluid divider base_color_background">
                                    <span class="top_arrow"></span>
                                    <h4 class="portfolio_title">
                                        <a href="#">John Smith</a>
                                    </h4>
                                </div>
                            </div>
                            <div class="row-fluid pull-left">
                                <div class="staff_content">
                                    <p class="staff_position">Creative Designer</p>
                                    <p class="staff_desc">
                                        Fugiat dapibus, tellus ac cursus commodo, mauesris condime ntum nibut fermentum mas justo sitters amet risus. Cras mattis cosi
                                    </p>
                                </div>
                                <div class="row-fluid pull-left">
                                    <div class="staff_links">
                                        <ul>
                                            <li>
                                                <a href="#">Facebook</a>
                                            </li>
                                            <li>
                                                <a href="#">Dribbble</a>
                                            </li>
                                            <li>
                                                <a href="#">Twitter</a>
                                            </li>
                                            <li>
                                                <a href="#"></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <span class="portfolio_shadow"></span>
                        </div>
                    </div>
                    <div class="span4 box_shadow box_layout">
                        <div class="staff_column">
                            <div class="recent_box">
                                <img alt="" src="images/2012/09/photodune-218412-work-from-home-s-23.jpg" />
                                <div class="row-fluid divider base_color_background">
                                    <span class="top_arrow"></span>
                                    <h4 class="portfolio_title">
                                        <a href="#">John Smith</a>
                                    </h4>
                                </div>
                            </div>
                            <div class="row-fluid pull-left">
                                <div class="staff_content">
                                    <p class="staff_position">Creative Designer</p>
                                    <p class="staff_desc">
                                        Fugiat dapibus, tellus ac cursus commodo, mauesris condime ntum nibut fermentum mas justo sitters amet risus. Cras mattis cosi
                                    </p>
                                </div>
                                <div class="row-fluid pull-left">
                                    <div class="staff_links">
                                        <ul>
                                            <li>
                                                <a href="#">Facebook</a>
                                            </li>
                                            <li>
                                                <a href="#">Dribbble</a>
                                            </li>
                                            <li>
                                                <a href="#">Twitter</a>
                                            </li>
                                            <li>
                                                <a href="#"></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <span class="portfolio_shadow"></span>
                        </div>
                    </div>
                    <div class="span4 box_shadow box_layout">
                        <div class="staff_column">
                            <div class="recent_box">
                                <img alt="" src="images/2012/10/photodune-202687-smiling-cute-business-woman-with-colleagues-at-the-back-m1.jpg" />
                                <div class="row-fluid divider base_color_background">
                                    <span class="top_arrow"></span>
                                    <h4 class="portfolio_title">
                                        <a href="#">John Smith</a>
                                    </h4>
                                </div>
                            </div>
                            <div class="row-fluid pull-left">
                                <div class="staff_content">
                                    <p class="staff_position">Creative Designer</p>
                                    <p class="staff_desc">
                                        Fugiat dapibus, tellus ac cursus commodo, mauesris condime ntum nibut fermentum mas justo sitters amet risus. Cras mattis cosi
                                    </p>
                                </div>
                                <div class="row-fluid pull-left">
                                    <div class="staff_links">
                                        <ul>
                                            <li>
                                                <a href="#">Facebook</a>
                                            </li>
                                            <li>
                                                <a href="#">Dribbble</a>
                                            </li>
                                            <li>
                                                <a href="#">Twitter</a>
                                            </li>
                                            <li>
                                                <a href="#"></a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <span class="portfolio_shadow"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div> -->
    </div>
</div>
<?php
require_once('partials/footer.php');
?>
<script>
    document.getElementById("nav2").classList.add('current-menu-item', 'current_page_item');
</script>