<?php


$productos = [
    '1' => [
        'nombre' => 'Bolsas chicas de Carbón 3, 4, 5, 8 y 10 kg',
        'categoria' => 1,
        'infocorta' => 'Carbón compuesto de Quebracho Blanco.',
        'info' => 'Bolsa de Carbón 4, 5, 8 y 10 kg <br> Quebracho Blanco',
        'precio' => '',
        'foto1' => 'images/productos/carbon/3.jpg',
        'foto2' => '',
    ],
    // '2' => [
    //     'nombre' => 'Bolsas de 35 kg',
    //     'categoria' => 1,
    //     'infocorta' => 'Carbón compuesto de Quebracho Blanco.',
    //     'info' => 'Bolsas de 35 kg <br> Quebracho Blanco',
    //     'precio' => '',
    //     'foto1' => 'images/productos/carbon/3.jpg',
    //     'foto2' => '',
    // ],
    // '3' => [
    //     'nombre' => '1000 kg',
    //     'categoria' => 1,
    //     'infocorta' => 'Carbón compuesto de Quebracho Blanco.',
    //     'info' => '1000 kg <br> Quebracho Blanco',
    //     'precio' => '',
    //     'foto1' => 'images/productos/carbon/4.jpg',
    //     'foto2' => '',
    // ],
    '4' => [
        'nombre' => 'Equipo Completo (28 toneladas)',
        'categoria' => 1,
        'infocorta' => 'Carbón compuesto de Quebracho Blanco.',
        'info' => 'Equipo Completo (28 toneladas) <br> Quebracho Blanco',
        'precio' => '',
        'foto1' => 'images/productos/carbon/4.jpg',
        'foto2' => '',
    ],
    '5' => [
        'nombre' => 'Bolsas de 10 y 15 kg',
        'categoria' => 2,
        'infocorta' => 'Leña de Quebracho Colorado.',
        'info' => 'Bolsas de 10kg <br> Quebracho Colorado',
        'precio' => '',
        'foto1' => 'images/construccion.png',
        'foto2' => '',
    ],
    '6' => [
        'nombre' => '100 kg',
        'categoria' => 2,
        'infocorta' => 'Leña de Quebracho Colorado.',
        'info' => '100 kg <br> Quebracho Colorado',
        'precio' => '',
        'foto1' => 'images/construccion.png',
        'foto2' => '',
    ],
    '7' => [
        'nombre' => '500 kg',
        'categoria' => 2,
        'infocorta' => 'Leña de Quebracho Colorado.',
        'info' => '500 kg <br> Quebracho Colorado',
        'precio' => '',
        'foto1' => 'images/construccion.png',
        'foto2' => '',
    ],
    '8' => [
        'nombre' => '1 tonelada',
        'categoria' => 2,
        'infocorta' => 'Leña de Quebracho Colorado.',
        'info' => '1 tonelada <br> Quebracho Colorado',
        'precio' => '',
        'foto1' => 'images/construccion.png',
        'foto2' => '',
    ],
    '10' => [
        'nombre' => 'Equipo Completo (28 toneladas)',
        'categoria' => 2,
        'infocorta' => 'Leña de Quebracho Colorado.',
        'info' => 'Equipo Completo (28 toneladas) <br> Quebracho Colorado',
        'precio' => '',
        'foto1' => 'images/construccion.png',
        'foto2' => '',
    ],
    '11' => [
        'nombre' => 'Puertas',
        'categoria' => 3,
        'infocorta' => 'Productos para el hogar.',
        'info' => '',
        'precio' => '',
        'foto1' => 'images/productos/hogar/puertas.jpg',
        'foto2' => '',
    ],
    '12' => [
        'nombre' => 'Ventanas',
        'categoria' => 3,
        'infocorta' => 'Productos para el hogar.',
        'info' => '',
        'precio' => '',
        'foto1' => 'images/productos/hogar/ventanas.jpg',
        'foto2' => '',
    ],
    '13' => [
        'nombre' => 'Pérgolas',
        'categoria' => 3,
        'infocorta' => 'Productos para el hogar.',
        'info' => '',
        'precio' => '',
        'foto1' => 'images/productos/hogar/pergolas1.jpg',
        'foto2' => 'images/productos/hogar/pergolas2.jpg',
    ],
    '14' => [
        'nombre' => 'Tablas',
        'categoria' => 3,
        'infocorta' => 'Productos para el hogar.',
        'info' => '',
        'precio' => '',
        'foto1' => 'images/productos/hogar/tabla.jpg',
        'foto2' => '',
    ],
    '15' => [
        'nombre' => 'Deck de Madera',
        'categoria' => 3,
        'infocorta' => 'Productos para el hogar.',
        'info' => '',
        'precio' => '',
        'foto1' => 'images/productos/hogar/deck.jpg',
        'foto2' => '',
    ],
    '16' => [
        'nombre' => 'Postes para alambrar',
        'categoria' => 3,
        'infocorta' => 'Productos para el hogar.',
        'info' => '',
        'precio' => '',
        'foto1' => 'images/productos/hogar/postes.jpg',
        'foto2' => '',
    ],
    '17' => [
        'nombre' => 'Durmientes y medio durmientes',
        'categoria' => 3,
        'infocorta' => 'Productos para el hogar.',
        'info' => '',
        'precio' => '',
        'foto1' => 'images/productos/hogar/durmiente.jpg',
        'foto2' => 'images/productos/hogar/mediodurmiente.jpg',
    ],
    '18' => [
        'nombre' => 'Tranqueras',
        'categoria' => 3,
        'infocorta' => 'Productos para el hogar.',
        'info' => '',
        'precio' => '',
        'foto1' => 'images/construccion.png',
        'foto2' => '',
    ],
];


$categoria = isset($_GET['c']) ? $_GET['c'] : null;
$categoria = intval($categoria);
$i = 0;
$categoria_productos = [];
while ($i <= count($productos)){
    if($productos[$i]['categoria'] == $categoria){
        $categoria_productos[] = $productos[$i];
    }
    $i++;
}

/* $categorias = [
    1 => [
        'nombre' => 'Leña',
        'desccorta' => 'Leña de Quebracho Colorado.',
        'descripcion' => 'Leña de Quebracho Colorado. Ofrecemos distintas medidas de fraccionamiento para cada necesidad, contamos con
        bolsas de fraccionadas medianas y grandes, también ofrecemos volúmenes para
        consumo hogareño como profesional.',
        'foto1' => 'images/productos/leña.jpg',
        'foto2' => '',
        'foto3' => '',
        'cantidad1' => 'Bolsa de 10 kg',
        'cantidad2' => 'Bolsa de 15 kg',
        'cantidad3' => '100 kg',
        'cantidad4' => '500 kg',
        'cantidad5' => '1000 kg',
        'cantidad6' => '3000 kg',
        'cantidad7' => 'Equipo COmpleto (28 Toneladas)',
    ],
    2 => [
        'nombre' => 'Carbon',
        'desccorta' => 'Leña de Quebracho Colorado.',
        'descripcion' => 'Carbón compuesto de Quebracho Blanco. Ofrecemos distintas medidas de fraccionamiento para cada necesidad, contamos con bolsas chicas, medianas y grandes, también ofrecemos cantidades para consumo de
        restaurantes y equipos completos.',
        'foto1' => 'images/productos/carbon.jpg',
        'foto2' => '',
        'foto3' => '',
        'cantidad1' => 'Bolsa de 3 kg',
        'cantidad2' => 'Bolsa de 4 kg',
        'cantidad3' => 'Bolsa de 5 kg',
        'cantidad4' => 'Bolsa de 8 kg',
        'cantidad5' => 'Bolsa de 10 kg',
        'cantidad6' => '1000 kg',
        'cantidad7' => 'Equipo COmpleto (28 Toneladas)',
    ],
    3 => [
        'nombre' => 'Hogar',
        'desccorta' => '',
        'descripcion' => 'Productos variados para tu hogar.',
        'foto1' => 'images/productos/hogar.jpg',
        'foto2' => '',
        'foto3' => '',
        'cantidad1' => '',
        'cantidad1' => '',
        'cantidad1' => '',
        'cantidad1' => '',
    ],
];

$id = isset($_GET['c']) ? $_GET['c'] : null;
$id = intval($id); */

require_once('partials/header.php');

?>


<div class="row-fluid page_title">

    <div class="container">
        <div class="span8">
            <h2 class="title_size">
                <span class="title_labeled"><?php echo ($productos[$categoria]['nombre']) ?></span>
            </h2>
            <h2 class="title_desc">
                <?php echo ($productos[$categoria]['infocorta']) ?>
            </h2>
            <!-- <h2 class="title_desc">Consectetur adipiscing elit aeneane lorem lipsum</h2> -->
        </div>
        <!-- <div class="span4">
            <div class="pull-right">

                <div id="search-3" class="widget title_widget widget_search">
                    <form class="form-inline" action="http://pixel.themeple.co/" id="searchform" method="get">

                        <input placeholder="Search type here.." class="span11 search-query" name="s" id="s" size="25"  type="text" />
                        <button type="submit" class="search_icon"></button>
                    </form>
                    <span class="seperator extralight-border"></span>
                </div>
            </div>
        </div> -->

    </div>
    <div class="row-fluid divider base_color_background">
        <div class="container">
            <span class="bottom_arrow"></span>
        </div>
    </div>

</div>
<div class="container shadow">
    <span class="bottom_shadow_full"></span>
</div>

<div class="row-fluid">
    <div class="container">
        <br>
        <div class="row-fluid distance_2 slider_container_portfolio">

            <img alt="" class="recent_box_image" src="<?php echo ($productos[$categoria]['foto1']) ?>"/>
            <div class="row-fluid divider base_color_background">
                <span class="bottom_arrow"></span>
            </div>
            <div class="row-fluid portfolio_info">
                <div class="portfolio_info_content span12 ">
                    <h2>Detalles:</h2>
                    <p>
                        <?php echo ($productos[$categoria]['info']) ?>
                    </p>
                </div>
            </div>
            <span class="bottom_shadow_full"></span>

        </div>
    </div>
    <div class="row-fluid pull-left">
        <div class="span12">
            <div class="container">
                <div class="row-fluid distance_1">
                    <div class="row-fluid text_bar_pattern">
                        <span class="right_arrow"></span>
                        <div class="content_bar">
                            <h1 class="banner_font">Te interesa?</h1>
                            <p class="banner_font">Dejenos su mensaje ahora.</p>
                        </div>
                        <a class="accordion-toggle button_bar" data-toggle="collapse" data-parent="#accordion1" href="#Accordion_1">Contactar</a>
                        <span class="banner_shadow"></span>
                    </div>
                    <div>
                        <div id="Accordion_1" class="accordion-body  collapse">
                            <div class="accordion-inner ">
                                <br><br><br>
                                <h3>Completa el formulario y seras contactado por uno de nuestros representantes:</h3>
                                <form id="contact-form" class="standard-form" action="contact.php" method="post">
                                <input class="span6" placeholder="Nombre" name="name"  type="text" id="name" value=""/>
                                <input class="span6" placeholder="E-Mail" name="email"  type="text" id="email" value=""/>
                                <input class="span12" placeholder="Teléfono" name="phone"  type="text" id="phone" value=""/>
                                <input type="hidden" value="<?php echo ($id) ?>">
                                <textarea class="span12" placeholder="Mensaje" name="message" cols="40" rows="7" id="message" ></textarea>
                                <p>
                                    <input type="submit"  value="Enviar" class="button_bar" />
                                </p>
                            </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <?php
    require_once('partials/footer.php');
    ?>
